#!/usr/bin/env bash

if [[ -n "$DEBUG" ]]; then
    set -x
fi

UPDATE_FLAG="--update"
if [[ "$UPDATE" = "false" || "$UPDATE" = "False" ]]; then
    UPDATE_FLAG="--no-update"
fi

UPDATE_LABELS_FLAG="--update-labels"
if [[ "$UPDATE_LABELS" = "false" || "$UPDATE_LABELS" = "False" ]]; then
    UPDATE_LABELS_FLAG="--no-update-labels"
fi

UPDATE_WIKI_FLAG="--update-wiki"
if [[ "$UPDATE_WIKI" = "false" || "$UPDATE_WIKI" = "False" ]]; then
    UPDATE_WIKI_FLAG="--no-update-wiki"
fi

MIN_SEVERITY_ARG="none"
if [[ -n "$MIN_SEVERITY" ]]; then
    MIN_SEVERITY_ARG="${MIN_SEVERITY}"
fi

IGNORE_LIST_ARG=".vulcanignore"
if [[ -n "$IGNORE_LIST" ]]; then
    IGNORE_LIST_ARG="${IGNORE_LIST}"
fi

DRY_RUN_FLAG="--no-dry-run"
if [[ "$DRY_RUN" = "true" || "$DRY_RUN" = "True" ]]; then
    DRY_RUN_FLAG="--dry-run"
fi

GENERATE_REPORT_FLAG="--generate-report"
if [[ "$GENERATE_REPORT" = "false" || "$GENERATE_REPORT" = "False" ]]; then
    GENERATE_REPORT_FLAG="--no-generate-report"
fi

export TRIVY_CACHE_DIR="${SCANNER_CACHE}"
export TRIVY_NO_PROGRESS="true"

trivy image \
    --format cyclonedx \
    --security-checks vuln \
    --output "${SBOM}" \
    "${SCAN_IMAGE}"

SBOM_TARGET_ARG=""
if [[ -n "$TARGET_IMAGE" ]]; then
    SBOM_TARGET="${SBOM%.*}-target.${SBOM##*.}"
    SBOM_TARGET_ARG="--sbom-target ${SBOM_TARGET}"
    if [[ -n "$DEBUG" ]]; then
        echo -e "TARGET_IMAGE: ${TARGET_IMAGE}"
        echo -e "SBOM_TARGET: ${SBOM_TARGET}"
    fi
    
    trivy image \
        --format cyclonedx \
        --security-checks vuln \
        --output "${SBOM_TARGET}" \
        "${TARGET_IMAGE}"
fi

"${HOME}"/.local/bin/poetry run vulcan \
    --gitlab-token "${GITLAB_TOKEN}" \
    --gitlab-project "${GITLAB_PROJECT}" \
    --sbom "${SBOM}" \
    ${SBOM_TARGET_ARG} \
    --min-severity "${MIN_SEVERITY_ARG}" \
    --ignore-list "${IGNORE_LIST_ARG}" \
    "${UPDATE_FLAG}" \
    "${UPDATE_LABELS_FLAG}" \
    "${DRY_RUN_FLAG}" \
    "${UPDATE_WIKI_FLAG}" \
    "${GENERATE_REPORT_FLAG}"
