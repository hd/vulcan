import unittest

from vulcan.markdown import MarkdownTemplate


class MarkdownTestCase(unittest.TestCase):
    def test_markdown_template_not_empty(self):
        template = MarkdownTemplate('vulcan/issue-template.md')
        self.assertIsNotNone(template.get_template())

    def test_markdown_template_always_same(self):
        template = MarkdownTemplate('vulcan/issue-template.md')
        t1 = template.get_template()
        t2 = template.get_template()
        self.assertIsNotNone(t1)
        self.assertIsNotNone(t2)
        self.assertEqual(t1, t2)
