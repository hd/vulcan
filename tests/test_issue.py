import unittest
from unittest.mock import MagicMock

from cyclonedx.model.vulnerability import Vulnerability

from vulcan.issue import format_title, format_description, search_vulnerability_issue
from vulcan.markdown import MarkdownTemplate
from vulcan.sbom import parse


class IssueTestCase(unittest.TestCase):

    def test_format_title(self):
        bom = parse("tests/sbom-full.json")
        vulnerability = bom.vulnerabilities[0]
        components = bom.get_affected_components_by(vulnerability)
        self.assertEqual("CVE-2004-0971: libgssapi-krb5-2 v1.18.3-6+deb11u1, libk5crypto3 v1.18.3-6+deb11u1, ...",
                         format_title(vulnerability, components))
        vulnerability = bom.vulnerabilities[1]
        components = bom.get_affected_components_by(vulnerability)
        self.assertEqual("CVE-2005-2541: tar v1.34+dfsg-1",
                         format_title(vulnerability, components))
        vulnerability = bom.vulnerabilities[2]
        components = bom.get_affected_components_by(vulnerability)
        self.assertEqual("CVE-2007-5686: login v1:4.8.1-1, passwd v1:4.8.1-1",
                         format_title(vulnerability, components))

    def test_format_description(self):
        bom = parse("tests/sbom-full.json")
        metadata = bom.metadata
        vulnerability = bom.vulnerabilities[0]
        components = bom.get_affected_components_by(vulnerability)

        template = MarkdownTemplate('vulcan/issue-template.md')
        temp = template.get_template()
        description = format_description(temp, metadata, vulnerability, components)
        self.assertIsNotNone(description)

    def test_search_vulnerability_issue_not_found(self):
        issue = search_vulnerability_issue(issues=[
            MagicMock(attributes={"title": "qwe"}),
            MagicMock(attributes={"title": "asd"}),
            MagicMock(attributes={"title": "zxc"})
        ], vulnerability=Vulnerability(id="abc"))
        self.assertIsNone(issue)

    def test_search_vulnerability_issue_full_match(self):
        issue = search_vulnerability_issue(issues=[
            MagicMock(attributes={"title": "abc"}),
            MagicMock(attributes={"title": "def"}),
            MagicMock(attributes={"title": "gfi"})
        ], vulnerability=Vulnerability(id="abc"))
        self.assertIsNotNone(issue)

    def test_search_vulnerability_issue_partial_match(self):
        issue = search_vulnerability_issue(issues=[
            MagicMock(attributes={"title": "abc:123"}),
            MagicMock(attributes={"title": "def:456"}),
            MagicMock(attributes={"title": "gfi:789"})
        ], vulnerability=Vulnerability(id="abc"))
        self.assertIsNotNone(issue)


if __name__ == '__main__':
    unittest.main()
