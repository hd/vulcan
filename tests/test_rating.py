import unittest
from decimal import Decimal

from vulcan.rating import _pick_single, RatingValue


class RatingTestCase(unittest.TestCase):
    def test_pick_single_on_empty_list_returns_none(self):
        self.assertIsNone(_pick_single(None))
        self.assertIsNone(_pick_single([]))

    def test_pick_single_on_unsorted_list_returns_greatest(self):
        self.assertEqual("CVSSv31", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv31", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
        ]).method)

        self.assertEqual("CVSSv3", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
        ]).method)

        self.assertEqual("CVSSv2", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv2", source="test", severity="info", score=Decimal(0)),
        ]).method)

    def test_pick_single_on_multiple_entries_returns_greatest(self):
        self.assertEqual("CVSSv31", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv31", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv31", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
        ]).method)

        self.assertEqual("CVSSv3", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv3", source="test", severity="info", score=Decimal(0)),
        ]).method)

        self.assertEqual("CVSSv2", _pick_single([
            RatingValue(method="OWASP", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv2", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="other", source="test", severity="info", score=Decimal(0)),
            RatingValue(method="CVSSv2", source="test", severity="info", score=Decimal(0)),
        ]).method)


if __name__ == '__main__':
    unittest.main()
