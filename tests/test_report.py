import unittest

from vulcan.report import Report


class ReportTestCase(unittest.TestCase):
    def test_report_write(self):
        report = Report("write_test.md", "test content")
        report.write_to("reports/test")

        with open("reports/test/write_test.md", "r") as file:
            content = file.read()

        self.assertIsNotNone(content)
        self.assertEqual("test content", content)
