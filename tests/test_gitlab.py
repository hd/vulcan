import unittest
from unittest.mock import Mock, MagicMock

import responses
from requests import Response

from vulcan.gitlab import GitlabProject


def response():
    r = Response()
    r.headers = {"Content-Type": "application/json"}
    r.status_code = 200
    r._content = "{}"
    return r


class GitlabTestCase(unittest.TestCase):

    @responses.activate
    def test_get_gitlab_project(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNotNone(gl.get_gitlab_project())
        self.assertEqual(gl.get_gitlab_project().get_id(), 42)
        self.assertEqual(gl.get_gitlab_project().attributes["name"], "project1")

    @responses.activate
    def test_list_issues(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/issues",
                      json=[{
                          "iid": 420,
                          "title": "issue1",
                      }],
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNotNone(gl.list_issues())
        self.assertEqual(1, len(gl.list_issues()))
        self.assertEqual(420, gl.list_issues()[0].get_id())
        self.assertEqual("42", gl.list_issues()[0].attributes["project_id"])
        self.assertEqual("issue1", gl.list_issues()[0].attributes["title"])

    @responses.activate
    def test_create_issue_for(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        payload = {
            "iid": 420,
            "title": "title1",
            'description': "description1",
            'confidential': True,
        }
        responses.add(responses.POST,
                      "https://gitlab.com/api/v4/projects/42/issues",
                      json=payload,
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        created = gl.create_issue_for(payload)
        self.assertIsNotNone(created)
        self.assertEqual(420, created.get_id())
        self.assertEqual("42", created.attributes["project_id"])
        self.assertEqual("title1", created.attributes["title"])
        self.assertEqual("description1", created.attributes["description"])
        self.assertEqual(True, created.attributes["confidential"])

    @responses.activate
    def test_update_issue_for(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        issue = MagicMock(get_id=Mock(return_value=420))
        payload = {
            "iid": 420,
            "title": "title2",
            'description': "description2",
            'confidential': True,
        }
        responses.add(responses.PUT,
                      "https://gitlab.com/api/v4/projects/42/issues/420",
                      json=payload,
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        created = gl.update_issue_for(issue, payload)
        self.assertIsNotNone(created)
        self.assertEqual(420, created["iid"])
        self.assertEqual("title2", created["title"])
        self.assertEqual("description2", created["description"])
        self.assertEqual(True, created["confidential"])

    @responses.activate
    def test_read_wiki(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/wikis/foo",
                      json={
                          "content": "foo page",
                          "format": "markdown",
                          "slug": "foo",
                          "title": "Foo",
                          "encoding": "UTF-8"
                      },
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNotNone(gl.read_wiki('foo'))
        self.assertEqual("foo", gl.read_wiki('foo').get_id())
        self.assertEqual("foo page", gl.read_wiki('foo').attributes["content"])
        self.assertEqual("markdown", gl.read_wiki('foo').attributes["format"])
        self.assertEqual("foo", gl.read_wiki('foo').attributes["slug"])
        self.assertEqual("Foo", gl.read_wiki('foo').attributes["title"])
        self.assertEqual("UTF-8", gl.read_wiki('foo').attributes["encoding"])

    @responses.activate
    def test_read_missing_wiki(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/wikis/not-found",
                      json={
                          "content": "not found",
                      },
                      status=404)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNone(gl.read_wiki('not-found'))

    @responses.activate
    def test_create_wiki(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.POST,
                      "https://gitlab.com/api/v4/projects/42/wikis",
                      json={
                          "content": "bar page",
                          "format": "markdown",
                          "slug": "bar",
                          "title": "Bar",
                          "encoding": "UTF-8"
                      },
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNotNone(gl.create_wiki('bar', 'bar page'))
        self.assertEqual("bar", gl.create_wiki('bar', 'bar page').get_id())
        self.assertEqual("bar page", gl.create_wiki('bar', 'bar page').attributes["content"])
        self.assertEqual("markdown", gl.create_wiki('bar', 'bar page').attributes["format"])
        self.assertEqual("bar", gl.create_wiki('bar', 'bar page').attributes["slug"])
        self.assertEqual("Bar", gl.create_wiki('bar', 'bar page').attributes["title"])
        self.assertEqual("UTF-8", gl.create_wiki('bar', 'bar page').attributes["encoding"])

    @responses.activate
    def test_update_wiki(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.PUT,
                      "https://gitlab.com/api/v4/projects/42/wikis/baz",
                      json={
                          "content": "baz page",
                          "format": "markdown",
                          "slug": "baz",
                          "title": "Baz",
                          "encoding": "UTF-8"
                      },
                      status=200)
        gl = GitlabProject(project_id=42, token='abc')
        self.assertIsNotNone(gl)
        self.assertIsNotNone(gl.update_wiki('Baz', 'baz page'))


if __name__ == '__main__':
    unittest.main()
