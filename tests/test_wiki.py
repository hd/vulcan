import unittest
from urllib.parse import quote

from vulcan.markdown import MarkdownTemplate
from vulcan.sbom import parse
from vulcan.wiki import render_dependencies_wiki, Dependency


class IssueTestCase(unittest.TestCase):

    def test_render_dependencies_wiki(self):
        bom = parse("tests/sbom-full.json")
        template = MarkdownTemplate('vulcan/dependencies-template.md')
        description = render_dependencies_wiki(template=template, sbom=bom)
        self.assertIsNotNone(description)

    def test_dependency_status_ok_without_vulnerabilities(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0], vulnerabilities=[])
        self.assertEqual("ok", dep.status_text)
        self.assertEqual("brightgreen", dep.status_color)

    def test_dependency_status_critical(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0],
                         vulnerabilities=[vul for vul in bom.vulnerabilities if vul.id in ['CVE-2022-29155', 'CVE-2022-23219']])
        self.assertEqual(quote("2H | 0M | 0L"), dep.status_text)
        self.assertEqual("darkred", dep.status_color)

    def test_dependency_status_high(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0], vulnerabilities=[vul for vul in bom.vulnerabilities if vul.id == 'CVE-2022-29458'])
        self.assertEqual(quote("1H | 0M | 0L"), dep.status_text)
        self.assertEqual("red", dep.status_color)

    def test_dependency_status_medium(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0], vulnerabilities=[vul for vul in bom.vulnerabilities if vul.id == 'CVE-2022-0563'])
        self.assertEqual(quote("0H | 1M | 0L"), dep.status_text)
        self.assertEqual("yellow", dep.status_color)

    def test_dependency_status_low(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0], vulnerabilities=[vul for vul in bom.vulnerabilities if vul.id == 'CVE-2021-4209'])
        self.assertEqual(quote("0H | 0M | 1L"), dep.status_text)
        self.assertEqual("blue", dep.status_color)

    def test_dependency_status_combined(self):
        bom = parse("tests/sbom-full.json")
        dep = Dependency(component=bom.components[0], vulnerabilities=[vul for vul in bom.vulnerabilities if vul.id in
                                                                       ['CVE-2022-29155', 'CVE-2022-23219', 'CVE-2021-4209',
                                                                        'CVE-2022-0563', 'CVE-2021-4160']])
        self.assertEqual(quote("2H | 1M | 2L"), dep.status_text)
        self.assertEqual("darkred", dep.status_color)


if __name__ == '__main__':
    unittest.main()
