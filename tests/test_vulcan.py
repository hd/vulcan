import sys
import unittest
from decimal import Decimal
from unittest.mock import patch, Mock

import responses
from cyclonedx.model.vulnerability import Vulnerability, VulnerabilityRating, VulnerabilitySeverity, \
    VulnerabilityScoreSource, \
    VulnerabilitySource
from gitlab.v4.objects import ProjectIssue

from vulcan import vulcan
from vulcan.gitlab import GitlabProject
from vulcan.markdown import MarkdownTemplate
from vulcan.rating import vulnerability_rating
from vulcan.severity import Severity
from vulcan.vulcan import Vulcan, run_vulcan, create_context


class VulcanTestCase(unittest.TestCase):
    @patch.object(sys, 'argv', [
        "self_prog",
        "--sbom", "abc",
        "--gitlab-token", "def",
        "--gitlab-project", "123"
    ])
    def test_vulcan_args(self):
        v = Vulcan()
        self.assertIsNotNone(v)
        self.assertIsNotNone(v.arguments)
        self.assertEqual("abc", v.arguments.sbom)
        self.assertEqual("def", v.arguments.gitlab_token)
        self.assertEqual(123, v.arguments.project_id)

    @patch.object(vulcan, "create_context", autospec=True)
    @patch.object(sys, 'argv', [
        "self_prog",
        "--sbom", "abc",
        "--gitlab-token", "def",
        "--gitlab-project", "123"
    ])
    def test_run_vulcan(self, mock_context_create):
        run_vulcan()
        mock_context_create.assert_called()

    def test_create_context(self):
        self.assertIsNotNone(create_context())

    def test_read_sbom(self):
        ctx = create_context().read_sbom("tests/sbom-full.json")
        self.assertIsNotNone(ctx)
        self.assertIsNotNone(ctx.sbom)
        self.assertIsNotNone(ctx.vulnerabilities)
        self.assertIsNotNone(ctx.components)
        self.assertEqual(71, len(ctx.vulnerabilities))
        self.assertEqual(148, len(ctx.components))

    def test_empty_sbom_target(self):
        ctx = create_context().read_sbom_target(None)
        self.assertIsNotNone(ctx)
        self.assertIsNone(ctx.sbom_target)
        self.assertIsNone(ctx.vulnerabilities_target)
        self.assertIsNone(ctx.components_target)

    def test_read_sbom_target(self):
        ctx = create_context().read_sbom_target("tests/sbom-full.json")
        self.assertIsNotNone(ctx)
        self.assertIsNotNone(ctx.sbom_target)
        self.assertIsNotNone(ctx.vulnerabilities_target)
        self.assertIsNotNone(ctx.components_target)
        self.assertEqual(71, len(ctx.vulnerabilities_target))
        self.assertEqual(148, len(ctx.components_target))

    def test_set_project(self):
        project = GitlabProject(project_id=42, token='abc')
        ctx = create_context().set_project(project)
        self.assertIsNotNone(ctx)
        self.assertEqual(project, ctx.project)

    def test_set_issue_template(self):
        issue_template = Mock()
        ctx = create_context().set_issue_template(issue_template)
        self.assertIsNotNone(ctx)
        self.assertEqual(issue_template, ctx.issue_template)

    def test_set_dependencies_template(self):
        deps_template = Mock()
        ctx = create_context().set_dependencies_template(deps_template)
        self.assertIsNotNone(ctx)
        self.assertEqual(deps_template, ctx.dependencies_template)

    @responses.activate
    def test_read_issues(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/issues",
                      json=[{
                          "iid": 420,
                          "title": "issue1",
                      }],
                      status=200)
        project = GitlabProject(project_id=42, token='abc')
        ctx = create_context() \
            .set_project(project) \
            .read_issues()
        self.assertIsNotNone(ctx)
        self.assertEqual(1, len(ctx.issues))
        self.assertEqual(420, ctx.issues[0].get_id())
        self.assertEqual("issue1", ctx.issues[0].attributes["title"])

    @responses.activate
    def test_create_new_issues(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/issues",
                      json=[{
                          "iid": 420,
                          "title": "issue1",
                      }],
                      status=200)
        create = responses.post("https://gitlab.com/api/v4/projects/42/issues",
                                json={"iid": "test"},
                                status=200)
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_issue_template(MarkdownTemplate('vulcan/issue-template.md'))
        ctx.create_list = [
            Vulnerability(id="abc"),
            Vulnerability(id="def"),
        ]
        ctx = ctx \
            .set_project(project) \
            .create_new_issues()
        self.assertIsNotNone(ctx)
        self.assertEqual(len(ctx.create_list), create.call_count)

    @responses.activate
    def test_update_existing_issues(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42/issues",
                      json=[{
                          "iid": 420,
                          "title": "issue1",
                      }],
                      status=200)
        update = responses.put("https://gitlab.com/api/v4/projects/42/issues/test",
                               json={"iid": "test"},
                               status=200)
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_issue_template(MarkdownTemplate('vulcan/issue-template.md'))
        issue1 = ProjectIssue(manager=Mock(), attrs={"iid": "test", "project_id": 42, "title": "abc", })
        issue2 = ProjectIssue(manager=Mock(), attrs={"iid": "test", "project_id": 42, "title": "def", })
        issue1.__dict__['_parent_attrs'] = {}
        issue2.__dict__['_parent_attrs'] = {}
        ctx.update_list = [
            (Vulnerability(id="abc"), issue1),
            (Vulnerability(id="def"), issue2),
        ]
        ctx = ctx \
            .set_project(project) \
            .update_existing_issues()
        self.assertIsNotNone(ctx)
        self.assertEqual(len(ctx.update_list), update.call_count)

    def test_build_create_and_update_vulnerability_lists(self):
        ctx = create_context().read_sbom("tests/sbom-full.json")
        ctx.issues = []
        self.assertIsNotNone(ctx.build_create_and_update_vulnerability_lists())
        self.assertGreaterEqual(len(ctx.create_list), 70)
        self.assertEqual([], ctx.update_list)

    def test_filter_vulnerabilities_with_severity(self):
        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .filter_vulnerabilities(Severity.high, 'invalid-list')
        self.assertLessEqual(len(ctx.vulnerabilities), 40)
        for vulnerability in ctx.vulnerabilities:
            rating = vulnerability_rating(vulnerability)
            self.assertGreaterEqual(rating.severity.priority, Severity.high.priority)

    def test_filter_vulnerabilities_with_list(self):
        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .filter_vulnerabilities(Severity.none, '.vulcanignore')
        for vulnerability in ctx.vulnerabilities:
            self.assertNotEqual('CVE-2011-4116', vulnerability.id)

    def test_filter_vulnerabilities_with_severity_and_list(self):
        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .filter_vulnerabilities(Severity.high, '.vulcanignore')
        self.assertLessEqual(len(ctx.vulnerabilities), 40)
        for vulnerability in ctx.vulnerabilities:
            rating = vulnerability_rating(vulnerability)
            self.assertGreaterEqual(rating.severity.priority, Severity.high.priority)
            self.assertNotEqual('CVE-2011-4116', vulnerability.id)

    @responses.activate
    def test_upsert_project_labels_creates_new_labels(self):
        responses.get("https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.get("https://gitlab.com/api/v4/projects/42/labels",
                      json=[],
                      status=200)
        create = responses.post("https://gitlab.com/api/v4/projects/42/labels",
                                json={},
                                status=200)
        project = GitlabProject(project_id=42, token='abc')
        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_project(project)
        ctx.create_list = [Vulnerability(
            ratings=[VulnerabilityRating(
                source=VulnerabilitySource(name="test"),
                score=Decimal(9.9),
                severity=VulnerabilitySeverity.CRITICAL,
                method=VulnerabilityScoreSource.CVSS_V3,
            )]
        )]
        ctx.update_list = []
        ctx.list_ratings() \
            .upsert_project_labels(True)
        self.assertIsNotNone(ctx)
        self.assertEqual(2, create.call_count)

    @responses.activate
    def test_upsert_project_labels_updates_existing_labels(self):
        responses.get("https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        responses.get("https://gitlab.com/api/v4/projects/42/labels",
                      json=[{
                          "name": "CVSSv3/critical"
                      }],
                      status=200)
        create = responses.post("https://gitlab.com/api/v4/projects/42/labels",
                                json={},
                                status=200)
        update = responses.put("https://gitlab.com/api/v4/projects/42/labels",
                               json={},
                               status=200)
        project = GitlabProject(project_id=42, token='abc')
        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_project(project)
        vul = Vulnerability(ratings=[
            VulnerabilityRating(source=VulnerabilitySource(name="test"), score=Decimal(9.9), severity=VulnerabilitySeverity.CRITICAL,
                                method=VulnerabilityScoreSource.CVSS_V3, )])
        ctx.create_list = []
        ctx.update_list = [(vul, None)]
        ctx.list_ratings() \
            .upsert_project_labels(True)
        self.assertIsNotNone(ctx)
        self.assertEqual(1, create.call_count)
        self.assertEqual(1, update.call_count)

    @responses.activate
    def test_upsert_dependencies_wiki_page(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        read = responses.add(responses.GET,
                             "https://gitlab.com/api/v4/projects/42/wikis/dependencies",
                             json={
                                 "content": "foo page",
                                 "format": "markdown",
                                 "slug": "foo",
                                 "title": "Foo",
                                 "encoding": "UTF-8"
                             },
                             status=200)
        update = responses.add(responses.PUT,
                               "https://gitlab.com/api/v4/projects/42/wikis/dependencies",
                               json={
                                   "content": "baz page",
                                   "format": "markdown",
                                   "slug": "baz",
                                   "title": "Baz",
                                   "encoding": "UTF-8"
                               },
                               status=200)
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_dependencies_template(MarkdownTemplate('vulcan/dependencies-template.md'))
        ctx = ctx \
            .set_project(project) \
            .upsert_dependencies_wiki()
        self.assertIsNotNone(ctx)
        self.assertEqual(1, read.call_count)
        self.assertEqual(1, update.call_count)

    @responses.activate
    def test_upsert_dependencies_wiki_page_without_change(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        read = responses.add(responses.GET,
                             "https://gitlab.com/api/v4/projects/42/wikis/dependencies",
                             json={
                                 "content": "foo\n",
                                 "format": "markdown",
                                 "slug": "foo",
                                 "title": "Foo",
                                 "encoding": "UTF-8"
                             },
                             status=200)
        update = responses.add(responses.PUT,
                               "https://gitlab.com/api/v4/projects/42/wikis/dependencies",
                               json={
                                   "content": "foo\n",
                                   "format": "markdown",
                                   "slug": "baz",
                                   "title": "Baz",
                                   "encoding": "UTF-8"
                               },
                               status=200)
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_dependencies_template(MarkdownTemplate('tests/foo-template.md'))
        ctx = ctx \
            .set_project(project) \
            .upsert_dependencies_wiki()
        self.assertIsNotNone(ctx)
        self.assertEqual(1, read.call_count)
        self.assertEqual(0, update.call_count)

    @responses.activate
    def test_upsert_dependencies_wiki_page_creates_wiki(self):
        responses.add(responses.GET,
                      "https://gitlab.com/api/v4/projects/42",
                      json={
                          "id": 42,
                          "name": "project1",
                      },
                      status=200)
        read = responses.add(responses.GET,
                             "https://gitlab.com/api/v4/projects/42/wikis/dependencies",
                             json={
                                 "content": "not found",
                             },
                             status=404)
        create = responses.add(responses.POST,
                               "https://gitlab.com/api/v4/projects/42/wikis",
                               json={
                                   "content": "baz page",
                                   "format": "markdown",
                                   "slug": "baz",
                                   "title": "Baz",
                                   "encoding": "UTF-8"
                               },
                               status=200)
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_dependencies_template(MarkdownTemplate('vulcan/dependencies-template.md'))
        ctx = ctx \
            .set_project(project) \
            .upsert_dependencies_wiki()
        self.assertIsNotNone(ctx)
        self.assertEqual(1, read.call_count)
        self.assertEqual(1, create.call_count)

    def test_dry_run_should_not_call_api(self):
        project = GitlabProject(project_id=42, token='abc')

        ctx = create_context() \
            .read_sbom("tests/sbom-full.json") \
            .set_project(project) \
            .set_issue_template(MarkdownTemplate('vulcan/issue-template.md')) \
            .set_dependencies_template(MarkdownTemplate('vulcan/dependencies-template.md')) \
            .read_issues(False) \
            .filter_vulnerabilities(Severity.none, 'missing-file') \
            .build_create_and_update_vulnerability_lists() \
            .list_ratings() \
            .upsert_project_labels(False) \
            .update_existing_issues(False) \
            .create_new_issues(False) \
            .upsert_dependencies_wiki(False)
        self.assertIsNotNone(ctx)

    def test_generate_reports(self):
        ctx = create_context()
        ctx.add_report("test/generate_reports_test.md", "test content 42")
        ctx.generate_reports(True)

        with open("reports/test/generate_reports_test.md", "r") as file:
            content = file.read()

        self.assertIsNotNone(content)
        self.assertEqual("test content 42", content)


if __name__ == '__main__':
    unittest.main()
