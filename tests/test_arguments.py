import unittest

from vulcan.arguments import Arguments
from vulcan.severity import Severity


class ArgumentsTestCase(unittest.TestCase):
    def test_no_args_exits(self):
        with self.assertRaises(SystemExit):
            Arguments([])

    def test_essential_args(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertIsNotNone(args)
        self.assertEqual("abc", args.sbom)
        self.assertEqual("def", args.gitlab_token)
        self.assertEqual(123, args.project_id)

    def test_sbom_is_required(self):
        with self.assertRaises(SystemExit):
            args = Arguments([
                "--gitlab-token", "def",
                "--gitlab-project", "123"
            ])
            self.assertIsNotNone(args)
            self.assertIsNone(args.sbom)
            self.assertEqual("def", args.gitlab_token)
            self.assertEqual(123, args.project_id)

    def test_gitlab_token_is_required(self):
        with self.assertRaises(SystemExit):
            args = Arguments([
                "--sbom", "abc",
                "--gitlab-project", "123"
            ])
            self.assertIsNotNone(args)
            self.assertEqual("abc", args.sbom)
            self.assertIsNone(args.gitlab_token)
            self.assertEqual(123, args.project_id)

    def test_gitlab_project_is_required(self):
        with self.assertRaises(SystemExit):
            args = Arguments([
                "--sbom", "abc",
                "--gitlab-token", "def",
            ])
            self.assertIsNotNone(args)
            self.assertEqual("abc", args.sbom)
            self.assertIsNone(args.project_id)
            self.assertEqual("def", args.gitlab_token)

    def test_update_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertTrue(args.update)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--update"
        ])
        self.assertTrue(args.update)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--no-update"
        ])
        self.assertFalse(args.update)

    def test_update_labels_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertTrue(args.update_labels)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--update-labels"
        ])
        self.assertTrue(args.update_labels)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--no-update-labels"
        ])
        self.assertFalse(args.update_labels)

    def test_update_wiki_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertTrue(args.update_wiki)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--update-wiki"
        ])
        self.assertTrue(args.update_wiki)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--no-update-wiki"
        ])
        self.assertFalse(args.update_wiki)

    def test_min_severity_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertEqual(args.min_severity, Severity.none)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--min-severity", 'medium',
        ])
        self.assertEqual(args.min_severity, Severity.medium)

        with self.assertRaises(SystemExit):
            Arguments([
                "--sbom", "abc",
                "--gitlab-token", "def",
                "--gitlab-project", "123",
                "--min-severity", 'nonsense',
            ])

    def test_dry_run_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertFalse(args.dry_run)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--dry-run"
        ])
        self.assertTrue(args.dry_run)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--no-dry-run"
        ])
        self.assertFalse(args.dry_run)

    def test_dry_run_does_not_require_token_or_project(self):
        args = Arguments([
            "--sbom", "abc",
            "--dry-run"
        ])
        self.assertTrue(args.dry_run)

    def test_ignore_list_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertEqual(args.ignore_list, '.vulcanignore')

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--ignore-list", 'some-list',
        ])
        self.assertEqual(args.ignore_list, 'some-list')

    def test_generate_report_arg(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertTrue(args.generate_report)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--generate-report"
        ])
        self.assertTrue(args.generate_report)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--no-generate-report"
        ])
        self.assertFalse(args.generate_report)

    def test_sbom_target(self):
        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123"
        ])
        self.assertIsNone(args.sbom_target)

        args = Arguments([
            "--sbom", "abc",
            "--gitlab-token", "def",
            "--gitlab-project", "123",
            "--sbom-target", "ghi",
        ])
        self.assertEqual(args.sbom_target, "ghi")
