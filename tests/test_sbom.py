import unittest

from cyclonedx.model.vulnerability import Vulnerability, VulnerabilityRating, VulnerabilitySeverity, VulnerabilityScoreSource

from vulcan.sbom import parse


class SbomTestCase(unittest.TestCase):
    def test_parse_full_file(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom)
        self.assertIsNotNone(bom.components)
        self.assertIsNotNone(bom.vulnerabilities)
        self.assertGreaterEqual(len(bom.components), 140)
        self.assertGreaterEqual(len(bom.vulnerabilities), 70)

    def test_parse_components(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom)
        self.assertIsNotNone(bom.components)
        self.assertGreaterEqual(len(bom.components), 140)
        comp = bom.components[0]
        self.assertIsNotNone(comp)
        self.assertIsNotNone(comp.purl)
        self.assertIsNotNone(comp.type)

    def test_parse_vulnerabilities(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom)
        self.assertIsNotNone(bom.vulnerabilities)
        self.assertGreaterEqual(len(bom.vulnerabilities), 70)
        vul: Vulnerability = bom.vulnerabilities[0]
        self.assertIsNotNone(vul)
        self.assertIsNotNone(vul.id)
        self.assertIsNotNone(vul.description)
        self.assertIsNotNone(vul.published)
        self.assertIsNotNone(vul.updated)
        self.assertIsNotNone(vul.source)
        self.assertIsNotNone(vul.source.name)
        self.assertIsNotNone(vul.source.url)
        self.assertGreaterEqual(len(vul.advisories), 9)
        vul = bom.vulnerabilities[2]
        self.assertGreaterEqual(len(vul.cwes), 1)

    def test_get_affected_components_by_vulnerability(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom)
        self.assertGreaterEqual(len(bom.get_affected_components_by(bom.vulnerabilities[0])), 2)
        self.assertGreaterEqual(len(bom.get_affected_components_by(bom.vulnerabilities[1])), 1)
        self.assertGreaterEqual(len(bom.get_affected_components_by(bom.vulnerabilities[2])), 2)
        self.assertGreaterEqual(len(bom.get_affected_components_by(bom.vulnerabilities[3])), 2)

    def test_parse_vulnerability_ratings(self):
        bom = parse("tests/sbom-full.json")
        vul = bom.vulnerabilities[0]
        self.assertIsNotNone(vul.ratings)
        self.assertEqual(len(vul.ratings), 2)
        rating: VulnerabilityRating = vul.ratings[0]
        self.assertIsNotNone(rating)
        self.assertIsNotNone(rating.source)
        self.assertEqual("nvd", rating.source.name)
        self.assertEqual(2.1, rating.score)
        self.assertEqual(VulnerabilitySeverity.INFO, rating.severity)
        self.assertEqual(VulnerabilityScoreSource.CVSS_V2, rating.method)
        self.assertIsNotNone(rating.vector)

    def test_metadata(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom.metadata)
        self.assertIsNotNone(bom.metadata.timestamp)
        self.assertIsNotNone(bom.metadata.component)
        self.assertEqual("postgres:14.0", bom.metadata.component.name)
        self.assertEqual("container", bom.metadata.component.type)
        self.assertGreaterEqual(len(bom.metadata.tools), 1)

    def test_get_component_vulnerabilities(self):
        bom = parse("tests/sbom-full.json")
        self.assertIsNotNone(bom)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[0].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[1].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[2].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[3].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[4].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[5].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[6].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[7].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[8].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[9].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[10].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[11].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[12].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[13].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[14].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[15].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[16].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[17].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[18].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[19].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[20].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[21].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[22].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[23].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[24].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[25].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[26].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[27].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[28].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[29].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[30].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[31].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[32].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[33].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[34].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[35].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[36].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[37].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[38].bom_ref)), 3)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[39].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[40].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[41].bom_ref)), 12)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[42].bom_ref)), 12)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[43].bom_ref)), 12)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[44].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[45].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[46].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[47].bom_ref)), 1)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[48].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[49].bom_ref)), 0)
        self.assertEqual(len(bom.get_component_vulnerabilities(bom.components[50].bom_ref)), 1)


if __name__ == '__main__':
    unittest.main()
