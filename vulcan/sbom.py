import json
from datetime import datetime
from typing import List, Optional, Iterable, Dict

from cyclonedx.model import XsUri, Tool
from cyclonedx.model.bom import Bom, BomMetaData
from cyclonedx.model.component import Component, ComponentType
from cyclonedx.model.impact_analysis import ImpactAnalysisAffectedStatus
from cyclonedx.model.vulnerability import Vulnerability, VulnerabilityAdvisory, VulnerabilitySource, BomTarget, \
    BomTargetVersionRange, VulnerabilityRating, VulnerabilitySeverity, VulnerabilityScoreSource
from cyclonedx.parser import BaseParser
from dateutil.parser import isoparse
from packageurl import PackageURL
from sortedcontainers import SortedSet


class Bom2(Bom):
    _component_refs: Optional[Dict[str, Component]] = None
    _vulnerability_refs: Optional[Dict[str, List[Vulnerability]]] = None
    vulnerabilities: SortedSet[Vulnerability] = SortedSet()

    def get_affected_components_by(self, vulnerability: Vulnerability) -> List[Component]:
        result = []
        for affected in vulnerability.affects:
            comp = self.get_component(affected.ref)
            if comp:
                result.append(comp)
        return result

    def get_component(self, ref: str) -> Optional[Component]:
        refs = self.get_component_refs()
        return refs.get(ref)

    def get_component_refs(self) -> Dict[str, Component]:
        if self._component_refs is None:
            self._component_refs = dict()
            for comp in self.components:
                self._component_refs[f"{comp.bom_ref}"] = comp
        return self._component_refs

    def get_component_vulnerabilities(self, ref: str) -> List[Vulnerability]:
        refs = self.get_vulnerability_refs()
        try:
            vul_list = refs[f"{ref}"]
        except KeyError:
            vul_list = []

        return vul_list

    def get_vulnerability_refs(self):
        if self._vulnerability_refs is None:
            self._vulnerability_refs = dict()
            for vul in self.vulnerabilities:
                vul: Vulnerability = vul
                for target in vul.affects:
                    target: BomTarget = target
                    ref = f"{target.ref}"
                    if ref:
                        try:
                            ref_list = self._vulnerability_refs[ref]
                            ref_list.append(vul)
                        except KeyError:
                            ref_list = [vul]
                        self._vulnerability_refs[ref] = ref_list
        return self._vulnerability_refs

    @staticmethod
    def from_parser(parser: BaseParser) -> 'Bom2':
        bom = Bom2()
        bom.components.update(parser.get_components())
        if parser.__class__ == SBOMJsonParser:
            json_parser: SBOMJsonParser
            json_parser = parser
            bom.vulnerabilities.update(json_parser.get_vulnerabilities())
            bom.metadata = json_parser.get_metadata()
        return bom


class SBOMJsonParser(BaseParser):
    json_content: dict
    _vulnerabilities: List[Vulnerability] = []

    def __init__(self, json_content):
        super().__init__()
        self.vulnerabilities = []
        self.json_content = json_content
        self.get_components().extend(self.list_components())
        self.get_vulnerabilities().extend(self.list_vulnerabilities())

    def list_components(self) -> List[Component]:
        result = []
        comp: dict
        for comp in self.json_content["components"]:
            result.append(self.read_component(comp))
        return result

    def read_component(self, comp: dict) -> Component:
        return Component(
            name=comp.get("name"),
            version=comp.get("version"),
            type=self.read_component_type(comp.get("type")),
            bom_ref=comp.get("bom-ref"),
            purl=self.read_purl(comp),
        )

    def list_vulnerabilities(self) -> List[Vulnerability]:
        result = []
        vul: dict
        for vul in self.json_content.get("vulnerabilities"):
            result.append(Vulnerability(
                id=vul.get("id"),
                source=self.read_source(vul.get("source")),
                description=vul.get("description"),
                published=self.read_datetime(vul, "published"),
                updated=self.read_datetime(vul, "updated"),
                advisories=self.read_advisories(vul, "advisories"),
                affects=self.read_affects(vul, "affects"),
                cwes=vul.get("cwes"),
                ratings=self.read_ratings(vul, "ratings")
            ))
        return result

    def get_vulnerabilities(self) -> List[Vulnerability]:
        return self._vulnerabilities

    @staticmethod
    def read_component_type(param: str) -> ComponentType:
        return ComponentType[param.upper().replace('-', '_', 1)] if param else None

    @staticmethod
    def read_purl(comp: dict) -> Optional[PackageURL]:
        try:
            purl_str = comp["purl"]
            purl = PackageURL.from_string(purl=purl_str)
            return purl
        except KeyError:
            return None
        except ValueError:
            return None

    @staticmethod
    def read_datetime(vul: dict, key: str) -> Optional[datetime]:
        try:
            val = vul.get(key)
            if val:
                return isoparse(val)
        except KeyError:
            return None

    @staticmethod
    def read_advisories(vul: dict, key: str) -> Optional[Iterable[VulnerabilityAdvisory]]:
        result = []
        try:
            advisories = vul.get(key)
            if advisories:
                for a in advisories:
                    result.append(VulnerabilityAdvisory(url=XsUri(uri=a["url"])))
        except KeyError:
            pass
        return result

    @staticmethod
    def read_source(param: dict) -> VulnerabilitySource:
        return VulnerabilitySource(name=param["name"], url=XsUri(uri=param["url"]))

    def read_affects(self, vul: dict, key: str) -> Optional[Iterable[BomTarget]]:
        result = []
        try:
            affects = vul.get(key)
            if affects:
                for a in affects:
                    result.append(BomTarget(ref=a["ref"], versions=self.read_affects_versions(a["versions"])))
        except KeyError:
            pass
        return result

    def read_affects_versions(self, param: List[dict]) -> Optional[Iterable[BomTargetVersionRange]]:
        result = []
        for a in param:
            result.append(BomTargetVersionRange(version=a["version"], status=self.impact_analysis_affected_status(a["status"])))
        return result

    @staticmethod
    def impact_analysis_affected_status(param: str) -> Optional[ImpactAnalysisAffectedStatus]:
        return ImpactAnalysisAffectedStatus[param.upper()]

    def get_metadata(self) -> BomMetaData:
        json_metadata: dict = self.json_content["metadata"]
        md = BomMetaData(
            component=self.read_component(json_metadata["component"]),
            tools=self.read_tools(json_metadata["tools"]),
        )
        md.timestamp = self.read_datetime(json_metadata, "timestamp")
        return md

    @staticmethod
    def read_tools(param: List[dict]) -> Optional[Iterable[Tool]]:
        result = []
        for t in param:
            result.append(Tool(vendor=t["vendor"], name=t["name"], version=t["version"]))
        return result

    def read_ratings(self, vul: dict, key: str) -> Optional[Iterable[VulnerabilityRating]]:
        result = []
        try:
            ratings = vul.get(key)
            if ratings:
                for vrt in ratings:
                    source = VulnerabilitySource(
                        name=vrt["source"].get("name"),
                        url=XsUri(uri=vrt["source"].get("url")) if vrt["source"].get("url") else None,
                    )
                    severity = VulnerabilitySeverity[vrt.get("severity").upper()] if vrt.get("severity") else None
                    method = self.read_score(vrt.get("method"))
                    result.append(VulnerabilityRating(
                        source=source,
                        score=vrt.get("score"),
                        severity=severity,
                        method=method,
                        vector=vrt.get("vector"),
                    ))
        except KeyError:
            pass
        return result

    @staticmethod
    def read_score(score: str) -> Optional[VulnerabilityScoreSource]:
        for s in VulnerabilityScoreSource.__members__:
            if VulnerabilityScoreSource[s].value == score:
                return VulnerabilityScoreSource[s]
        return None


def parse(sbom_file: str) -> Bom2:
    with open(sbom_file) as f:
        js = json.load(f)
        parser = SBOMJsonParser(js)
        bom: Bom2 = Bom2.from_parser(parser=parser)
        return bom
