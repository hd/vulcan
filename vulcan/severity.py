from enum import Enum


class Severity(Enum):
    priority: int
    color: str

    critical = "critical", 5, 'darkred',
    high = "high", 4, 'red',
    medium = "medium", 3, 'yellow',
    info = "info", 2, 'yellow',
    low = "low", 1, 'blue',
    none = "none", 0, 'brightgreen',

    def __new__(cls, value, prio, color) -> 'Severity':
        obj = object.__new__(cls)
        # first value is canonical value
        obj._value_ = value
        obj.priority = prio
        obj.color = color
        return obj

    def __str__(self):
        return self.value
