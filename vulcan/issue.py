from typing import List, Optional

import chevron
from cyclonedx.model.bom import BomMetaData
from cyclonedx.model.component import Component
from cyclonedx.model.vulnerability import Vulnerability
from gitlab.v4.objects import ProjectIssue

from vulcan.rating import vulnerability_rating


def format_description(template: str, metadata: BomMetaData, vulnerability: Vulnerability, components: List[Component]) -> str:
    ratings = vulnerability_rating(vulnerability)
    data = {
        'metadata': metadata,
        'vulnerability': vulnerability,
        'components': components,
        'ratings': ratings,
    }
    return chevron.render(template, data)


def name_version(comp: Component) -> Optional[str]:
    return f"{comp.name} v{comp.version}" if comp else None


def format_title(vulnerability: Vulnerability, affected_components: List[Component]) -> str:
    components = affected_components if len(affected_components) <= 2 else affected_components[:2]
    affects = ", ".join(map(name_version, components))
    affects = affects if len(affected_components) <= 2 else f"{affects}, ..."
    return f"{vulnerability.id}: {affects}"


def search_vulnerability_issue(issues: List[ProjectIssue], vulnerability: Vulnerability) -> Optional[ProjectIssue]:
    for issue in issues:
        title: str = issue.attributes["title"]
        if vulnerability.id in title:
            return issue
    return None


def issue_labels(metadata: BomMetaData, vulnerability: Vulnerability) -> list[str]:
    labels = []
    _append_target_label(labels, metadata)
    _append_rating_label(labels, vulnerability)
    return labels


def _append_target_label(labels, metadata):
    labels.append(f"target:{metadata.component.name}")


def _append_rating_label(labels, vulnerability):
    rating = vulnerability_rating(vulnerability)
    if rating is not None:
        labels.append(f"{rating.method}/{rating.severity}")


class Issue:
    name: str
    title: str
    description: str
    labels: list[str]

    def __init__(self, name: str, title: str, description: str, labels: list[str]):
        self.name = name
        self.title = title
        self.description = description
        self.labels = labels

    def payload(self):
        return {
            'title': self.title,
            'description': self.description,
            'confidential': True,
            'labels': self.labels
        }


def issue_for(issue_template, metadata: BomMetaData, vulnerability: Vulnerability, affected_components: List[Component]) -> Issue:
    labels = issue_labels(metadata, vulnerability)
    template = issue_template.get_template()
    return Issue(
        name=f"{vulnerability.id}",
        title=format_title(vulnerability, affected_components),
        description=format_description(template, metadata, vulnerability, affected_components),
        labels=labels)
