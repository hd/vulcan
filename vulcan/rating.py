from decimal import Decimal

from cyclonedx.model.vulnerability import Vulnerability, VulnerabilityRating
from sortedcontainers import SortedSet

from vulcan.severity import Severity

_rating_method_priorities = {
    "CVSSv31": 6000,
    "CVSSv3": 5000,
    "CVSSv2": 4000,
    "OWASP": 3000,
    "Open FAIR": 2000,
    "other": 1000,
}


class RatingValue:
    source: str
    method: str
    score: Decimal
    severity: Severity

    def __init__(self, source: str, method: str, score: Decimal, severity: str):
        self.source = source
        self.method = method
        self.score = score
        self.severity = Severity(severity)

    @property
    def severity_color(self) -> str:
        return self.severity.color

    @property
    def priority(self) -> int:
        return self.severity.priority

    @property
    def is_valid(self) -> bool:
        return self.source is not None \
               and self.method is not None \
               and self.severity is not None \
               and self.score is not None


def rating_value(rating: VulnerabilityRating) -> RatingValue or None:
    if rating.source and rating.score and rating.severity and rating.method:
        return RatingValue(rating.source.name, f"{rating.method}", rating.score, f"{rating.severity}")
    return None


def _rating_values(ratings: SortedSet[VulnerabilityRating]) -> list[RatingValue]:
    values = []
    for rating in ratings:
        value = rating_value(rating)
        if value:
            values.append(value)
    return values


def _pick_single(rating_values: list[RatingValue]) -> RatingValue or None:
    if rating_values:
        valid_ratings = [r for r in rating_values if r.is_valid]
        if valid_ratings and len(valid_ratings) > 0:
            sorted_list = sorted(valid_ratings, key=lambda rv: _rating_method_priorities[rv.method], reverse=True)
            return sorted_list[0]
    return None


def vulnerability_rating(vulnerability: Vulnerability) -> RatingValue or None:
    rating_values = _rating_values(vulnerability.ratings)
    picked = _pick_single(rating_values)
    return picked
