
{{#ratings}}
![rating from {{source}}({{method}}): {{score}}](https://img.shields.io/badge/{{method}}/{{source}}-{{score}}-{{severity_color}})
{{/ratings}}

### Details

{{vulnerability.description}}

---

| details       | ...                                                           |
|---------------|---------------------------------------------------------------|
| **component** | `{{metadata.component.name}} ({{metadata.component.type}})`   |
| **source:**   | [{{vulnerability.source.name}}]({{vulnerability.source.url}}) |
| **published** | `{{vulnerability.published}}`                                 |
| **updated**   | `{{vulnerability.updated}}`                                   |
| **scanner**   | `{{metadata.tools.0.name}} {{metadata.tools.0.version}}`      |
| **scan data** | `{{metadata.timestamp}}`                                      |

---

### Affected components

{{#components}}
- {{name}} v{{version}}
{{/components}}

### Advisories

<details><summary>Click to expand</summary>

{{#vulnerability.advisories}}
- [{{url}}]({{url}})
{{/vulnerability.advisories}}

</details>

### CWEs

<details><summary>Click to expand</summary>

{{#vulnerability.cwes}}
- [CWE-{{.}}](https://cwe.mitre.org/data/definitions/{{.}}.html)
{{/vulnerability.cwes}}

</details>

---

_This issue is created by [vulcan🖖](https://gitlab.com/hd/vulcan)_
