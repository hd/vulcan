from typing import List, Tuple, Set, Dict

from cyclonedx.model.component import Component
from cyclonedx.model.vulnerability import Vulnerability
from gitlab.v4.objects import ProjectIssue, ProjectLabel

from vulcan.arguments import Arguments
from vulcan.gitlab import GitlabProject
from vulcan.issue import search_vulnerability_issue, issue_for
from vulcan.markdown import MarkdownTemplate
from vulcan.rating import vulnerability_rating, RatingValue
from vulcan.report import Report
from vulcan.sbom import parse, Bom2
from vulcan.severity import Severity
from vulcan.wiki import render_dependencies_wiki


def is_different(issue: ProjectIssue, payload: Dict[str, any]) -> bool:
    return issue.attributes['title'] != payload['title'] \
        or issue.attributes['description'] != payload['description']


def read_ignore_list(ignore_list_file: str) -> list[str]:
    try:
        with open(ignore_list_file, 'r') as fd:
            result = [e for e in fd.read().split('\n') if e not in '']
    except IOError:
        result = []
    return result


class VulcanContext:
    sbom: Bom2 or None
    sbom_target: Bom2 or None
    project: GitlabProject or None
    issue_template: MarkdownTemplate or None
    dependencies_template: MarkdownTemplate or None
    components: list[Component] or None
    components_target: list[Component] or None
    issues: list[ProjectIssue] or None
    vulnerabilities: list[Vulnerability] or None
    vulnerabilities_target: list[Vulnerability] or None
    create_list: list[Vulnerability] or None
    update_list: list[Tuple[Vulnerability, ProjectIssue]] or None
    ratings: Set[RatingValue] or None
    reports: list[Report] or None

    def __init__(self):
        self.sbom = None
        self.sbom_target = None
        self.project = None
        self.issue_template = None
        self.dependencies_template = None
        self.issues = None
        self.components = None
        self.components_target = None
        self.vulnerabilities: None
        self.vulnerabilities_target: None
        self.create_list = None
        self.update_list = None
        self.ratings = None
        self.reports = None

    def read_sbom(self, sbom_file: str) -> 'VulcanContext':
        print(f"reading SBOM {sbom_file} ...")
        self.sbom = parse(sbom_file)
        self.components = self.sbom.components
        self.vulnerabilities = self.sbom.vulnerabilities
        print(f"SBOM with {len(self.components)} components and {len(self.sbom.vulnerabilities)} vulnerabilities.")
        return self

    def read_sbom_target(self, sbom_file: str or None) -> 'VulcanContext':
        if sbom_file:
            print(f"reading SBOM target {sbom_file} ...")
            self.sbom_target = parse(sbom_file)
            self.components_target = self.sbom_target.components
            self.vulnerabilities_target = self.sbom_target.vulnerabilities
            print(f"SBOM with {len(self.components_target)} components and {len(self.vulnerabilities_target)} vulnerabilities.")
        else:
            print("no SBOM target file specified.")
            self.sbom_target = None
            self.components_target = None
            self.vulnerabilities_target = None
        return self

    def set_project(self, project: GitlabProject) -> 'VulcanContext':
        self.project = project
        return self

    def set_issue_template(self, issue_template: MarkdownTemplate) -> 'VulcanContext':
        self.issue_template = issue_template
        return self

    def set_dependencies_template(self, dependencies_template: MarkdownTemplate) -> 'VulcanContext':
        self.dependencies_template = dependencies_template
        return self

    def read_issues(self, do_read_issues: bool = True) -> 'VulcanContext':
        if do_read_issues:
            print("reading existing issues...")
            issues = self.project.list_issues()
            print(f"project has {len(issues)} issues.")
            self.issues = issues
        else:
            self.issues = []
        return self

    def build_create_and_update_vulnerability_lists(self) -> 'VulcanContext':
        create_list: List[Vulnerability] = []
        update_list: List[Tuple[Vulnerability, ProjectIssue]] = []
        for vulnerability in self.vulnerabilities:
            matching_issue = search_vulnerability_issue(self.issues, vulnerability)
            if matching_issue:
                update_list.append((vulnerability, matching_issue))
            else:
                create_list.append(vulnerability)
        self.create_list = create_list
        self.update_list = update_list
        return self

    def update_existing_issues(self, do_update_issues: bool = True, do_generate_report: bool = True) -> 'VulcanContext':
        issue_list = self.update_list
        if do_update_issues:
            print(f"updating {len(issue_list)} existing issues...")

        for vulnerability_issue in issue_list:
            vulnerability = vulnerability_issue[0]
            existing_issue = vulnerability_issue[1]
            issue = issue_for(self.issue_template,
                              metadata=self.sbom.metadata,
                              vulnerability=vulnerability,
                              affected_components=self.sbom.get_affected_components_by(vulnerability),
                              )
            payload = issue.payload()

            if do_update_issues:
                if is_different(existing_issue, payload):
                    existing_issue = self.project.update_issue_for(existing_issue, payload)
                    print(f"issue #{existing_issue['iid']} for {vulnerability.id} updated.")
                else:
                    print(f"issue #{existing_issue['iid']} for {vulnerability.id} is unchanged.")

            if do_generate_report:
                self.add_report(f"vulnerabilities/{issue.name}.md", issue.description)

        return self

    def create_new_issues(self, do_create_issues: bool = True, do_generate_report: bool = True) -> 'VulcanContext':
        issue_list = self.create_list
        if do_create_issues:
            print(f"creating {len(issue_list)} issues for new vulnerabilities...")

        for vulnerability in issue_list:
            issue = issue_for(
                self.issue_template,
                metadata=self.sbom.metadata,
                vulnerability=vulnerability,
                affected_components=self.sbom.get_affected_components_by(vulnerability),
            )

            if do_create_issues:
                new_issue = self.project.create_issue_for(issue.payload())
                print(f"issue #{new_issue.get_id()} for {vulnerability.id} created.")

            if do_generate_report:
                self.add_report(f"vulnerabilities/{issue.name}.md", issue.description)

        return self

    def list_ratings(self) -> 'VulcanContext':
        ratings = set()
        for vul in self.create_list:
            rating = vulnerability_rating(vul)
            if rating is not None:
                ratings.add(rating)

        for vul_issue in self.update_list:
            vul = vul_issue[0]
            rating = vulnerability_rating(vul)
            if rating is not None:
                ratings.add(rating)
        self.ratings = ratings
        return self

    def upsert_project_labels(self, do_update_labels: bool = True) -> 'VulcanContext':
        if do_update_labels and self.ratings:
            project_labels: list[ProjectLabel] = self.project.list_labels()

            # metadata labels
            name = f"target:{self.sbom.metadata.component.name}"
            color = "gray"
            label = next(filter(lambda x: x.get_id() == name, project_labels), None)
            if label:
                if label.attributes.get("color") != color:
                    # update
                    label.color = color
                    label.save()
            else:
                # insert
                self.project.create_label_for({
                    "color": color,
                    "name": name,
                })

            # rating labels
            for rating in self.ratings:
                label = next(filter(lambda x: x.get_id() == f"{rating.method}/{rating.severity}", project_labels), None)
                if label:
                    if label.attributes.get("color") != rating.severity_color or label.attributes.get("priority") != rating.priority:
                        # update
                        label.color = rating.severity_color
                        label.priority = rating.priority
                        label.save()
                else:
                    # insert
                    self.project.create_label_for({
                        "color": rating.severity_color,
                        "name": f"{rating.method}/{rating.severity}",
                        "priority": rating.priority,
                    })
        return self

    def filter_vulnerabilities(self, min_severity: Severity, ignore_list_file: str) -> 'VulcanContext':
        vulnerabilities = self.vulnerabilities[:]
        ignore_list = read_ignore_list(ignore_list_file)
        if min_severity:
            for vul in self.vulnerabilities:
                rating = vulnerability_rating(vul)
                if rating is None or rating.severity.priority < min_severity.priority:
                    vulnerabilities.remove(vul)
                    continue
        if len(ignore_list) > 0:
            print("ignoring vulnerabilities:")
            print('\n'.join(ignore_list))
            vulnerabilities = [v for v in vulnerabilities if v.id not in ignore_list]
        self.vulnerabilities = vulnerabilities
        return self

    def upsert_dependencies_wiki(self, do_upsert_wiki: bool = True, do_generate_report: bool = True) -> 'VulcanContext':
        title = 'dependencies'
        content = render_dependencies_wiki(template=self.dependencies_template, sbom=self.sbom)

        if do_upsert_wiki:
            existing_page = self.project.read_wiki(title)
            if existing_page:
                if content != existing_page.content:
                    print(f"updating wiki {title}...")
                    self.project.update_wiki(title=title, new_content=content)
                else:
                    print(f"updating wiki {title} skipped: no change")
            else:
                print(f"creating wiki {title}...")
                self.project.create_wiki(title=title, content=content)
        if do_generate_report:
            self.add_report(f"wiki/{title}.md", content)
        return self

    def add_report(self, name, content):
        if self.reports is None:
            self.reports = []
        self.reports.append(Report(name=name, content=content))

    def generate_reports(self, do_generate_report: bool = True) -> 'VulcanContext':
        report_dir = 'reports'
        if do_generate_report:
            print(f"writing reports under {report_dir} ...")
            for report in self.reports:
                print(f"writing {report_dir}/{report.name} ...", end='')
                report.write_to(report_dir)
                print(" ✔")

        return self


def create_context() -> VulcanContext:
    return VulcanContext()


class Vulcan:
    _gitlab_project = GitlabProject

    def __init__(self):
        self._gitlab_project = None
        self._issue_template = MarkdownTemplate('vulcan/issue-template.md')
        self._dependencies_template = MarkdownTemplate('vulcan/dependencies-template.md')
        self.arguments = Arguments()
        self._gitlab_project = GitlabProject(
            token=self.arguments.gitlab_token,
            project_id=self.arguments.project_id,
        )

    def run(self):
        print("running vulcan🖖 with parameters:")
        print(self.arguments)

        create_context() \
            .read_sbom(self.arguments.sbom) \
            .read_sbom_target(self.arguments.sbom_target) \
            .set_project(self._gitlab_project) \
            .set_issue_template(self._issue_template) \
            .set_dependencies_template(self._dependencies_template) \
            .read_issues(not self.arguments.dry_run) \
            .filter_vulnerabilities(self.arguments.min_severity, self.arguments.ignore_list) \
            .build_create_and_update_vulnerability_lists() \
            .list_ratings() \
            .upsert_project_labels(not self.arguments.dry_run and self.arguments.update_labels) \
            .update_existing_issues(not self.arguments.dry_run, self.arguments.generate_report) \
            .create_new_issues(not self.arguments.dry_run, self.arguments.generate_report) \
            .upsert_dependencies_wiki(not self.arguments.dry_run, self.arguments.generate_report) \
            .generate_reports(self.arguments.generate_report)
        print("done.")


def run_vulcan():
    v = Vulcan()
    v.run()
