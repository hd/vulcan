import os


class Report:
    name: str
    content: str

    def __init__(self, name: str, content: str):
        self.name = name
        self.content = content

    def write_to(self, target_dir: str):
        filename = f"{target_dir}/{self.name}"
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'w') as file:
            file.write(f"{self.content}")
