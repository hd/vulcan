from urllib.parse import quote
from typing import List

import chevron
from cyclonedx.model.component import Component
from cyclonedx.model.vulnerability import Vulnerability

from vulcan.markdown import MarkdownTemplate
from vulcan.rating import RatingValue, vulnerability_rating
from vulcan.sbom import Bom2
from vulcan.severity import Severity


class DependencyStatus:
    _rating_sums: dict[Severity, int]

    def __init__(self, ratings: List[RatingValue]):
        self._highest_rating = Severity.none
        self._rating_sums = {
            Severity.critical: 0,
            Severity.high: 0,
            Severity.medium: 0,
            Severity.info: 0,
            Severity.low: 0,
            Severity.none: 0
        }
        for rating in ratings:
            severity = rating.severity
            if severity.priority > self._highest_rating.priority:
                self._highest_rating = severity
            try:
                self._rating_sums[severity] = self._rating_sums[severity] + 1
            except KeyError:
                self._rating_sums[severity] = 1

    @property
    def text(self) -> str:
        return f"{self._rating_sums[Severity.critical] + self._rating_sums[Severity.high]}H | " \
               f"{self._rating_sums[Severity.medium]}M | " \
               f"{self._rating_sums[Severity.low]}L"

    @property
    def color(self) -> str:
        return f"{self._highest_rating.color}"


class Dependency:
    _status: DependencyStatus or None
    component: Component

    def __init__(self, component: Component, vulnerabilities: List[Vulnerability]):
        self._status = None
        self.component = component
        self.vulnerabilities = vulnerabilities

    @property
    def name(self) -> str:
        return self.component.name

    @property
    def version(self) -> str:
        return self.component.version

    @property
    def purl(self) -> str:
        return self.component.purl

    @property
    def status_text(self) -> str:
        if len(self.vulnerabilities) == 0:
            return "ok"
        else:
            return quote(self.status.text)

    @property
    def status_color(self) -> str:
        if len(self.vulnerabilities) == 0:
            return Severity.none.color
        else:
            return self.status.color

    @property
    def _ratings(self) -> List[RatingValue]:
        return [rating for rating in
                [vulnerability_rating(vul) for vul in self.vulnerabilities]
                if rating is not None and rating.is_valid]

    @property
    def status(self) -> DependencyStatus:
        if self._status is None:
            self._status = DependencyStatus(self._ratings)
        return self._status


def render_dependencies_wiki(template: MarkdownTemplate, sbom: Bom2) -> str:
    metadata = sbom.metadata
    components = sorted(sbom.components, key=lambda d: f"{d.purl}")
    dependencies = map(lambda c: Dependency(component=c, vulnerabilities=sbom.get_component_vulnerabilities(c.bom_ref)), components)
    data = {
        'title': 'dependencies',
        'metadata': metadata,
        'dependencies': dependencies,
    }
    return chevron.render(template.get_template(), data)
