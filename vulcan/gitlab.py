from typing import Optional, List, Any
from urllib.parse import quote

from gitlab import Gitlab, GitlabGetError
from gitlab.v4.objects import Project, ProjectIssue, ProjectLabel, ProjectWiki


class GitlabProject:
    _project: Optional[Project]
    _gitlab: Gitlab

    def __init__(self, token: str, project_id: int):
        self._gitlab_project = None
        self._project_id = project_id
        self._gitlab = Gitlab(
            oauth_token=token
        )

    def get_gitlab_project(self) -> Project:
        if self._gitlab_project is None:
            self._gitlab_project = self._gitlab.projects.get(id=self._project_id)
        return self._gitlab_project

    def list_issues(self) -> List[ProjectIssue]:
        project = self.get_gitlab_project()
        issues = project.issues.list(all=True)
        return issues

    def list_labels(self) -> List[ProjectLabel]:
        project = self.get_gitlab_project()
        labels = project.labels.list(all=True)
        return labels

    def create_label_for(self, payload: dict[str, Any]) -> ProjectLabel:
        project = self.get_gitlab_project()
        label = project.labels.create(data=payload)
        return ProjectLabel(label.manager, label.attributes)

    def create_issue_for(self, payload: dict[str, Any]) -> ProjectIssue:
        project = self.get_gitlab_project()
        issue = project.issues.create(data=payload)
        return issue

    def update_issue_for(self, issue: ProjectIssue, payload: dict[str, Any]) -> dict[str, Any]:
        project = self.get_gitlab_project()
        updated = project.issues.update(id=issue.get_id(), new_data=payload)
        return updated

    def read_wiki(self, slug: str) -> ProjectWiki or None:
        try:
            project = self.get_gitlab_project()
            wiki = project.wikis.get(slug, lazy=False)
            return wiki
        except GitlabGetError:
            return None

    def create_wiki(self, title: str, content: str) -> ProjectWiki:
        project = self.get_gitlab_project()
        wiki = project.wikis.create({
            "content": content,
            "format": "markdown",
            "slug": quote(title).lower(),
            "title": title,
            "encoding": "UTF-8"
        })
        return wiki

    def update_wiki(self, title: str, new_content: str) -> ProjectWiki:
        project = self.get_gitlab_project()
        wiki = project.wikis.update(id=quote(title).lower(), new_data={
            "content": new_content,
        })
        return wiki
