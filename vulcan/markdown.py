from typing import Optional


class MarkdownTemplate:
    _template: Optional[str]

    def __init__(self, template_file: str):
        self._template_file = template_file
        self._template = None

    def get_template(self) -> str:
        if self._template is None:
            with open(self._template_file, 'r') as file:
                self._template = file.read()
        return self._template
