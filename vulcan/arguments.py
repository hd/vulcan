import argparse
import sys

from vulcan.severity import Severity


class ArgumentParser(argparse.ArgumentParser):

    def error(self, message):
        self.print_help(sys.stdout)
        self.exit(2, '%s: error: %s\n' % (self.prog, message))


class Arguments:
    gitlab_token: str
    project_id: int
    sbom: str
    sbom_target: str
    update: bool
    update_labels: bool
    update_wiki: bool
    min_severity: Severity
    dry_run: bool
    ignore_list: str
    generate_report: bool

    # noinspection PyTypeChecker
    def __init__(self, args=None):
        if args is None:
            args = sys.argv[1:]

        parser = ArgumentParser()
        parser.add_argument('--gitlab-token',
                            help='GitLab Token with api scope')
        parser.add_argument('--gitlab-project',
                            type=int, help='GitLab project id')
        parser.add_argument('--sbom', required=True,
                            help='Path to the SBOM file in CycloneDX format')
        parser.add_argument('--sbom-target', default=None,
                            help='Path to the target SBOM file in CycloneDX format. This will be used to create a diff analysis.')
        parser.add_argument('--update', action=argparse.BooleanOptionalAction, default=True,
                            help='Update existing issues')
        parser.add_argument('--update-labels', action=argparse.BooleanOptionalAction, default=True,
                            help='Update existing project labels')
        parser.add_argument('--update-wiki', action=argparse.BooleanOptionalAction, default=True,
                            help='Update project wiki pages')
        parser.add_argument('--min-severity', type=Severity, choices=list(Severity), default=Severity.none,
                            help='Minimum severity for vulnerabilities')
        parser.add_argument('--dry-run', action=argparse.BooleanOptionalAction, default=False,
                            help='Perform a dry run without updating the project')
        parser.add_argument('--ignore-list', default='.vulcanignore',
                            help='A line-break list of vulnerabilities (CVEs) to ignore during scan')
        parser.add_argument('--generate-report', action=argparse.BooleanOptionalAction, default=True,
                            help='Generate a local report in filesystem')

        try:
            args = parser.parse_args(args=args)
            self.gitlab_token = args.gitlab_token
            self.sbom = args.sbom
            self.sbom_target = args.sbom_target
            self.project_id = args.gitlab_project
            self.update = args.update
            self.update_labels = args.update_labels
            self.update_wiki = args.update_wiki
            self.min_severity = args.min_severity
            self.dry_run = args.dry_run
            self.ignore_list = args.ignore_list
            self.generate_report = args.generate_report
        except argparse.ArgumentError as e:
            parser.error(e.message)
        except argparse.ArgumentTypeError as e:
            parser.error(e.__str__())

        if not self.dry_run and (self.gitlab_token is None or self.project_id is None):
            parser.error("the following arguments are required if not running in dry mode:"
                         " --gitlab-token,"
                         " --gitlab-project"
                         )

    def __str__(self):
        return f"           sbom: {self.sbom}\n" \
               f"    sbom-target: {self.sbom_target}\n" \
               f" gitlab-project: {self.project_id}\n" \
               f"         update: {self.update}\n" \
               f"  update-labels: {self.update_labels}\n" \
               f"    update-wiki: {self.update_wiki}\n" \
               f"   min-severity: {self.min_severity}\n" \
               f"        dry-run: {self.dry_run}\n" \
               f"    ignore-list: {self.ignore_list}\n" \
               f"generate-report: {self.generate_report}\n"
