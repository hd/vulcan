# Scan Details

---

| details       | ...                                                           |
|---------------|---------------------------------------------------------------|
| **component** | `{{metadata.component.name}} ({{metadata.component.type}})`   |
| **scan time** | `{{metadata.timestamp}}`                                      |

# Dependency List

| status | name | version | purl |
|--------|------|---------|------|
{{#dependencies}}
| ![status](https://img.shields.io/badge/-{{status_text}}-{{status_color}}) | {{name}} | {{version}} | {{purl}} |
{{/dependencies}}
