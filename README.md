# vulcan 🖖

Vulnerability Management on Gitlab issues.

vulcan🖖 scans a given target (i.e. container image) and creates a Gitlab issue for every vulnerability on the project repository. Existing issues of a previous scan are updated.

## Features

- Performs a vulnerability scan on a given container image using the [Trivy scanner](https://aquasecurity.github.io/trivy/).
- Creates and updates [Gitlab issues](https://docs.gitlab.com/ee/user/project/issues/) on a given project
- Creates [confidential vulnerability issues](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html) which are only available to the project members/maintainers.
- Use [project labels](https://docs.gitlab.com/ee/user/project/labels.html) on issues to make issues easily manageable

## How to use

Include the [Gitlab CI template](https://docs.gitlab.com/ee/ci/yaml/#include):

```yaml
include:
  - remote: https://gitlab.com/hd/vulcan/-/blob/v0.4.0/gitlab-ci-template.yml
```

and define some variables according to your needs:

```yaml
variables:
  SCAN_IMAGE: postgres:14.0                     # -- image to be scanned (required)
  GITLAB_TOKEN: $GITLAB_TOKEN                   # -- Gitlab token with API read/write nature (required)
  GITLAB_PROJECT: $CI_PROJECT_ID                # -- Gitlab project ID (optional)
  TARGET_IMAGE: postgres:latest                 # -- target image to be considered for a diff (optional)
  SBOM: $CI_PROJECT_DIR/sbom.json               # -- SBOM report as artifact (optional)
  SBOM_TARGET: $CI_PROJECT_DIR/sbom_target.json # -- SBOM report as artifact (optional)
  MIN_SEVERITY: "high"                          # -- minimum severity rating for the vulnerabilities to include (optional)
                                                #       value of 'critical', 'high', 'medium', 'info', 'low', 'none'
  UPDATE: "false"                               # -- setting the value to 'false' or 'False' will disable issue update
  UPDATE_LABELS: "false"                        # -- setting the value to 'false' or 'False' will disable project label update
  UPDATE_WIKI: "false"                          # -- setting the value to 'false' or 'False' will disable wiki update
  DRY_RUN: "true"                               # -- setting the value to 'true' or 'True' will perform dry run where no issues, 
                                                #       labels or wiki is updated
  GENERATE_REPORT: "true"                       # -- setting the value to 'true' or 'True' will generate reports 
                                                #       under 'report/' directory 
  IGNORE_LIST: ".vulcanignore"                  # -- Vulnerability ignore list in a plain text file. See below for file contents.
```

### `IGNORE_LIST` with `.vulcanignore` file

A plain text file, containing new-line (lf) separated list of vulnerabilities (CVE-IDs) to ignore from the scan.

Example:

```text
CVE-2011-4116
```

## Results

### List of issues

![list of issues](docs/README-1.png)

### Issue with details

![issue with details](docs/README-2.png)

## CVSS Scoring Scales

Following CVSS Scoring scales are preferred in order of preference as long as they are provided:
 - [CVSSv31](https://www.first.org/cvss/calculator/3.1)
 - [CVSSv3](https://www.first.org/cvss/calculator/3.0)
 - [CVSSv2](https://nvd.nist.gov/vuln-metrics/cvss/v2-calculator)

The following scoring ranges are used among different scoring scales:

| **Score** 	 | **CVSSv2** 	                                           | **CVSSv31** / **CVSSv3** 	                                                    |
|-------------|--------------------------------------------------------|-------------------------------------------------------------------------------|
| 0.0       	 | ![low](https://img.shields.io/badge/-low-blue)         | ![none](https://img.shields.io/badge/-none-brightgreen)                     	 |
| 0.1 - 3.9 	 | ![low](https://img.shields.io/badge/-low-blue)         | ![low](https://img.shields.io/badge/-low-blue)                      	         |
| 4.0 - 6.9 	 | ![medium](https://img.shields.io/badge/-medium-yellow) | ![medium](https://img.shields.io/badge/-medium-yellow)                   	    |
| 7.0 - 8.9 	 | ![high](https://img.shields.io/badge/-high-red)        | ![high](https://img.shields.io/badge/-high-red)                     	         |
| 9.0 - 10  	 | ![high](https://img.shields.io/badge/-high-red)        | ![critical](https://img.shields.io/badge/-critical-darkred)                 	 |
