FROM aquasec/trivy:latest as trivy
RUN \
    trivy version && \
    ls -al /usr/local/bin/trivy && \
    ls -al contrib/*.tpl

#----------------------------
FROM python:3.10-slim

# trivy stuff ---------------
ENV SCANNER_CACHE=/scanner-cache
COPY --from=trivy /usr/local/bin/trivy /usr/local/bin/trivy
COPY --from=trivy contrib/*.tpl contrib/

# vulcan stuff --------------
RUN \
    DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
        curl && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sSL https://install.python-poetry.org | python3 -

COPY vulcan /vulcan
COPY \
    run-vulcan* \
    poetry.lock \
    pyproject.toml \
    /

RUN \
    export PATH="$HOME/.local/bin:$PATH" && \
    poetry install

ENTRYPOINT [ "/run-vulcan" ]
